import paramiko
import time
import sys
import threading

from forward import forward_tunnel

def forward(local_port, host, user, passwd, remote_port, ssh):
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

	try:
		print ('connecting to', host)
		client.connect(host,username=user, password=passwd,port=ssh)
		print ('connected')
	except Exception as e:
		print ('*** Failed to connect to %s:%d: %r' % ('*****', 22, e))
		#sys.exit(1)

	try:
		forward_tunnel(local_port, '127.0.0.1', remote_port, client.get_transport())
	except SystemExit:
		print ('C-c: Port forwarding stopped.')
		#sys.exit(0)

class Tunel(threading.Thread):
	def __init__(self, local_port, host, user, passwd, remote_port,ssh_port):
		threading.Thread.__init__(self)
		self.name = host
		self.host = host
		self.user = user
		self.passwd = passwd
		self.local = local_port
		self.remote = remote_port
		self.ssh_port = ssh_port
	def run(self):
		forward(self.local, self.host, self.user, self.passwd, self.remote, self.ssh_port)
	def getHost(self):
		return self.host

class ThreadManager():
	def __init__(self):
		self.lista =[]
	def connect(self,local_port, host, user, passwd, remote_port, ssh_port):
		w = Tunel(local_port, host, user, passwd, remote_port, ssh_port)
		self.lista.append(w)
		w.start()

ThreadMan = ThreadManager()
ThreadMan.connect(1234,'antivps.pl','dbshepherd','dbshepherd',443,22)
ThreadMan.connect(1235,'play.bloodfrontier.net','root','quinsucks666',5432,666)
#   	client.connect('antivps.pl', username='dbshepherd', password='dbshepherd')
#    	client.connect('play.bloodfrontier.net', username='root', password='quinsucks666', port=666)
time.sleep(5)

print(ThreadMan.lista)

for tunel in ThreadMan.lista:
	print(tunel.getHost(),tunel.name,tunel.isAlive())

print("Teraz już chodza tylko watki z tunelami")