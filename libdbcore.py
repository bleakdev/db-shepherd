from imp import find_module
import argparse
import sys

sys.path.append("dbmodules")
default_module = "test_mod"

def is_exist(module):
	try:
		find_module('dbmodules/'+module)
		return True
	except ImportError:
		return False
		
def get_module(module = default_module):
	return __import__(module)
	
def add_module(parser, module = default_module,):
	#get_module("test_mod").test_mod(parser)
	exec("get_module('{0}').{1}(parser)".format(module.lower(), module))
	
	
