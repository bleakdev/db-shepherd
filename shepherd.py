import argparse

import libdbcore

class AddModule(argparse.Action):
        def __call__(self,parser,namespace,values,option_string):
                if libdbcore.is_exist(values):
                    libdbcore.add_module(parser,values)
                else:
                    print("There is no module called", values)

parser = argparse.ArgumentParser(description="db")
parser.add_argument('-m', action=AddModule, help='use module')

args = parser.parse_known_args()
parser.parse_args(args[1])
##test
