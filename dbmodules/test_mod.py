from shutil import copyfile
from shutil import Error
import maindb



class Test_mod(maindb.MainDB):
	def __init__(self, parser):
                self.parser = parser
                self.add_action(self.copy, '-c', "Copy test", 2)
                self.add_action(self.copyssh, '-cs', "SSH copy", 2)
                self.add_action(self.show, '-s', "show test", '+')
                self.add_action(self.testssh, '-ts', "ssh test", 2)
                self.add_action(self.testssh, '-fs', "ssh test", 2)
		
	def copy(self, values):
		copyfile(values[0],values[1])
		
	def show(self, values):
		import alias
		try:
			ali = alias.Alias("conf.yaml")
		except alias.AliasError as e:
			print("No conf file")

		for x in values:
			print ("Getting", x)
			try:
				server = ali.show(x)
				print (server)
			except alias.AliasError:
				print ("there is no", x, "in conf...")

	def copyssh(self,arguments):

		import sys
		sys.path.append("..")
		import ssh
		import alias

		try:
			conf = alias.Alias("conf.yaml")
		except alias.AliasError as e:
			print (e)

		server_name = []
		path = []
		ssh_list = []	
		users = []

		for arg in arguments:
			try:
				server_name.append(arg.split(":")[0])
				path.append(arg.split(":")[1])
			except IndexError: #Pojawi się jak się nie uda podzielić = zle podany argument
				#rise sth
				exit()
		try:
			conf.show(server_name[0])
			conf.show(server_name[1])
		except alias.AliasError as e:
			print (e)
			#rise or sth
			exit()

		for server in server_name:
			user_dict = {}
			try:
				user_dict["name"] = conf.show(server)["connection"]["ssh"]["user"]
				user_dict["addr"] = conf.show(server)["connection"]["addres"]
			except KeyError as e:
				print ("Add", e, "to yaml file for", server)
				exit()
				
			try:
				user_dict["rsa"] = conf.show(server)["connection"]["ssh"]["rsa"]
			except KeyError as e:
				input_msg = "No rsa for " + server + " enter password: "
				user_dict["passwd"] = input(input_msg)

			users.append(user_dict)


		for user in users:
			temp_ssh = ssh.Ssh()
			try:
				temp_ssh.rsa_connect(user["addr"],user["name"],user["rsa"])
			except KeyError:
				temp_ssh.passwd_connect(user["addr"],user["name"],user["passwd"])
			ssh_list.append(temp_ssh)
		#pomyśleć jak rozwiązać z jednego na wiele
		ssh_list[0].copy_from(path[0], ".\\temp")
		ssh_list[1].copy_to('.\\temp', path[1])