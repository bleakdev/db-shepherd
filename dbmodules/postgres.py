import maindb

import sys
sys.path.append("..")

import alias
import psycopg2

class Postgres(maindb.MainDB):
	def __init__(self, parser):
		self.parser = parser
		self.add_action(self.query, '-q', "Query to database", 3)
		
	def query(self, values):
		ali = alias.Alias(values[0])
		adr = ali.show(values[1].split('.')[0])["connection"]["adress"]
		usr = ali.show(values[1].split('.')[0])[values[1].split('.')[1]]["user"]
		pwd = ali.show(values[1].split('.')[0])[values[1].split('.')[1]]["passwd"]
		db_name = ali.show(values[1].split('.')[0])[values[1].split('.')[1]]["name"]

		try:
			conn = psycopg2.connect(dbname=db_name,user=usr,host=adr,password=pwd, port=1234)
			cur = conn.cursor()
			cur.execute(values[2])
			rows = cur.fetchall()

			for row in rows:
				for cell in row:
					print(cell, "   ", end='')
				print("\n");
		except:
			print("Problem z połączeniem!")