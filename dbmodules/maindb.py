import argparse

class MainDB(object):
	def __init__(self, parser = None):
		pass
		
	def to_action(self, function):
		class Wrapper(argparse.Action):
			def __call__(self, parser, namespace, values, option_string):
				function(values)
		return Wrapper
				
	def add_action(self, fun, opt, help_text, n_args=1):
		if self.parser is not None:
			self.parser.add_argument(opt, action=self.to_action(fun), nargs=n_args, help=help_text, required=False)
