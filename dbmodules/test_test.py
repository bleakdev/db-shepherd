import unittest

import sys
sys.path.append("..")

import Test
from filecmp import cmp

class test_Test(unittest.TestCase):
	def test_copy(self):
		test = Test.Test(None)
		test.copy(["file.txt", "cpy_file.txt"])
		self.assertTrue(cmp("file.txt", "cpy_file.txt"))
		
unittest.main()